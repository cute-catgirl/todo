import { writable } from 'svelte/store';
import { browser } from '$app/environment';

export const todos = writable((browser && JSON.parse(localStorage.getItem('todotasks'))) || []);
todos.subscribe((val) => {
	if (browser) return (localStorage.todotasks = JSON.stringify(val));
});
